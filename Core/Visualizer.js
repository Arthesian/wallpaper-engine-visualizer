;
var modules = (function(ns, $, document){

    // Core visualizer object
    ns.Visualizer = function(settings){

        // Scope binding
        var _ = {};

        // Default settings
        var _DEFAULTS = {
            canvasId : "canvas",
            
            // Debug/Performance 
            enableFpsCounter : false,
            enableLogOutput : false,

            // Horizontal Mirror of screen
            enableHorizontalMirror : false,
            horizontalMirrorOffset : 0,
            horizontalMirrorAlpha : 0.5,
            
            // Vertical Mirror of screen
            enableVerticalMirror : false,
            verticalMirrorOffset : 0,
            verticalMirrorAlpha : 0.5
        }

        // Override default settings with provided settings
        _.settings = $.extend({}, _DEFAULTS, settings);

        // Variables that needs to be stored
        _.vars = {
            container : new createjs.Container(),
            canvas : null,
            stage : null,
            visuals : [],

            // Debug
            logs : [],
        }

        // Initialize
        var init = function(){

            // New container
            _.vars.container = new createjs.Container();

            // Add container to stage
            _.vars.stage.addChild(container);
        }

        // What should happend if the 'init' needs to be re-done
        _.reset = function(){
            
        }

        _.setTargetFPS = function(fpsTarget){
            // Set target fps
        }

        // Bind all event handlers
        _.bind = function(){
            
            // Bind error 
            var error = console.error;
            console.error = function(){
                var jsonString = JSON.stringify(arguments);
                var textObject = new createjs.Text("-- " + jsonString, "12px Arial", "#ff7700");
                
                _.vars.logs.push(textObject);

                if(_.vars.logs.length > 20){ _.vars.logs.shift();}

                error.apply(console, arguments);
            }
        }

        _.create = function(){

            // Create FPS counter
            _.createFPSCounter();

            // Create debug info window
            _.createDebugInfo();
        }

        _.createFPSCounter = function(){
            
            // FPS counter
            var containerFps = new createjs.Container();
        }

        _.createDebugInfo = function(){
            
            // Print log
            var containerLogs = new createjs.Container();

            _.vars.logs.forEach(function(textObject, i){
                
                textObject.y = i * 20;

                containerLogs.addChild(textObject);
            });

            _.vars.container.addChild(containerLogs);
        }

        // The update function is called every frame!
        _.update = function(){
    
            // Clear container
            _.vars.container.removeAllChildren();

            // Update the visuals
            _.vars.visuals.forEach(function(visual){
                visual.update();
            });

            // Call draw method that draws all the objects
            _.draw();

            // Call update on the stage ( the actual draw )
            _.vars.stage.update();
        }

        // Draw the visualizer to the screen ( most cpu intensive )
        _.draw = function(){

            if(_.settings.enableFpsCounter){
                // FPS counter
                _.createFPSCounter();
            }

            if(_.settings.enableLogOutput){
                // Debug info 
                _.createDebugInfo();
            }

            if(_.settings.enableHorizontalMirror) {

            }

            if(_.settings.enableVerticalMirror){

            }
        }
        
        // Call initialize
        init();
        
        // Return scope
        return _;
    }

    // Return namescape
    return ns;

})(modules || {}, jQuery, document);